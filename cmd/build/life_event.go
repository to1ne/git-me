package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"git-me.tech/pkg/gitme"
)

type EventDate string
type Slugs []string

// LifeEvent define a significant event in my life that's worth creating a
// commit for.
type LifeEvent struct {
	Branch  string    // when nil use branch of parent
	Slug    string    // slug used to reference this event as parent
	Parents Slugs     // slugs of parent commits
	Date    EventDate // date of life event
	Message string    // text used as commit message
	gitme.Oid
}

// Timestamp returns the date as a time.Time
func (e EventDate) Timestamp() time.Time {
	timestamp, err := time.Parse("2006-01-02", string(e))
	if err != nil {
		log.Fatalf("Failed to parse date %s: %v", string(e), err)
	}

	return timestamp
}

// ParentEvents returns the parent LifeEvents looked up by slug
func (event LifeEvent) ParentEvents() ([]LifeEvent, error) {
	var parents []LifeEvent

	for _, slug := range event.Parents {
		found := false
		for _, e := range lifeEvents {
			if slug == e.Slug {
				parents = append(parents, e)
				found = true
				break
			}
		}
		if !found {
			return nil, fmt.Errorf("Failed to find parent %s", slug)
		}
	}
	return parents, nil
}

func (event LifeEvent) Title() string {
	title, _, _ := strings.Cut(event.Message, "\n")
	return title
}

func (event LifeEvent) Body() string {
	_, body, _ := strings.Cut(event.Message, "\n\n")
	return body
}

// Commit creates commit for the current LifeEvent
func (event *LifeEvent) Commit(gitter gitme.Gitter) error {
	parents, err := event.ParentEvents()
	if err != nil {
		log.Fatalf("Could not lookup parents: %v", err)
	}

	var parentOids []gitme.Oid

	for _, p := range parents {
		parentOids = append(parentOids, p.Oid)
	}

	tree, err := gitter.DefaultTree()
	if err != nil {
		log.Fatalf("Could not get tree: %v", err)
	}

	event.Oid, err = gitter.CreateCommit(event.Date.Timestamp(), event.Message, tree, parentOids...)
	if err != nil {
		return fmt.Errorf("Failed to create commit at %s: %v", string(event.Date), err)
	}

	if event.Branch != "" {
		err = gitter.CreateBranch(event.Branch, event.Oid)
		if err != nil {
			return fmt.Errorf("Failed to create branch %s: %v", event.Branch, err)
		}
	}

	return nil
}
