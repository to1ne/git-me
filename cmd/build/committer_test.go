package main

import (
	"bytes"
	"os/exec"
	"testing"

	"git-me.tech/pkg/gitme"
	"github.com/stretchr/testify/require"
)

func TestWriteCommits(t *testing.T) {
	gitDir := t.TempDir()

	err := WriteCommits(gitDir)
	require.NoError(t, err, "Failed to write commits")

	t.Run("Includes all commits", func(t *testing.T) {
		cmd := exec.Command("git", "-C", gitDir, "log", "--format=%H")
		output, err := cmd.Output()
		require.NoError(t, err, "Failed to get git log")

		hashes := bytes.Fields(output)
		if len(hashes) != len(lifeEvents) {
			require.Len(t, lifeEvents, len(hashes))

			for _, event := range lifeEvents {
				found := false
				for _, h := range hashes {
					if gitme.Oid(h) == event.Oid {
						found = true
						break
					}
				}
				require.Truef(t, found, "Commit missing: %v", event.Slug)
			}
		}
	})
}
