package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
)

const plainLogFormat = `%Cred` +
	`%h` + // abbreviated commit hash
	`%Cgreen` +
	`%d` + // ref name
	`%Creset` +
	`%C(bold)` +
	` %s` + // subject
	`%Creset` +
	`%n` + // new line
	`%C(italic)` +
	`Date: ` +
	`%Cblue` +
	`%as` + // author date
	`%Creset` +
	`%n` + // new line
	`%b` // body

func plainFilename(name string) string {
	if name == "toon" {
		name = "index.html"
	}

	name = filepath.Join("public", "plain", name)
	os.MkdirAll(path.Dir(name), os.ModePerm)

	return name
}

func Plainize(repoPath, branch string) error {
	plainFile, err := os.Create(plainFilename(branch))
	if err != nil {
		return fmt.Errorf("Plainize: %v", err)
	}
	defer plainFile.Close()

	cmd := exec.Command("git",
		"-C", repoPath,
		"log",
		"--graph",
		"--color",
		"--date-order",
		"--pretty="+plainLogFormat,
		branch)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return fmt.Errorf("Plainize: %v", err)
	}

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("Plainize: %v", err)
	}

	if _, err := io.Copy(plainFile, stdout); err != nil {
		return fmt.Errorf("Plainize: %v", err)
	}

	if err := cmd.Wait(); err != nil {
		return fmt.Errorf("Plainize: %v", err)
	}

	if err = plainFile.Sync(); err != nil {
		return fmt.Errorf("Plainize: %v", err)
	}

	return nil

}
