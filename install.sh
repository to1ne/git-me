#!/usr/bin/env bash
set -e

DIR=$(mktemp --tmpdir --directory toon.XXXXX.git)

git clone --quiet https://git-me.tech ${DIR}
git -C ${DIR} config alias.me '!f() { git log --graph --date-order \
              --format="%Cred%h%Cgreen%d%Creset%C(bold) %s%Creset%n%C(italic)Date: %Cblue%as%Creset%n%b" \
              origin/${1:-$(git rev-parse --abbrev-ref HEAD)}; }; f'

echo "Repository set up at ${DIR}. Run the following command to use:"
echo
echo "    cd ${DIR} && git me"
