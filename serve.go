package main

import (
	"embed"
	"io/fs"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

//go:embed public/*
var content embed.FS

// plainTextAgents contains signatures of the plain-text agents.
var plainTextAgents = []string{
	"curl",
	"httpie",
	"lwp-request",
	"wget",
	"python-httpx",
	"python-requests",
	"openbsd ftp",
	"powershell",
	"fetch",
	"aiohttp",
	"http_get",
	"xh",
}

// isPlainTextAgent returns true if userAgent is a plain-text agent.
// Kindly borrowed from https://github.com/chubin/wttr.in
func isPlainTextAgent(userAgent string) bool {
	for _, signature := range plainTextAgents {
		if strings.Contains(userAgent, signature) {
			return true
		}
	}
	return false
}

var (
	opsProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "gitme_requests_total",
		Help: "The total number of requests",
	}, []string{"client"})
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3333"

	}

	publicFs, err := fs.Sub(content, "public")
	if err != nil {
		log.Fatalf("cannot sub public: %v", err)
	}
	publicServer := http.FileServer(http.FS(publicFs))

	gitFs, err := fs.Sub(publicFs, "toon.git")
	if err != nil {
		log.Fatalf("cannot sub toon.git: %v", err)
	}
	gitServer := http.FileServer(http.FS(gitFs))

	plainFs, err := fs.Sub(publicFs, "plain")
	if err != nil {
		log.Fatalf("cannot sub plain: %v", err)
	}
	plainServer := http.FileServer(http.FS(plainFs))

	http.Handle("/metrics", promhttp.Handler())

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		userAgent := strings.ToLower(req.Header.Get("User-Agent"))

		switch {
		case strings.Contains(userAgent, "git"):
			opsProcessed.With(prometheus.Labels{"client": "git"}).Inc()
			gitServer.ServeHTTP(w, req)

		case isPlainTextAgent(userAgent):
			opsProcessed.With(prometheus.Labels{"client": "plain"}).Inc()
			plainServer.ServeHTTP(w, req)

		default:
			opsProcessed.With(prometheus.Labels{"client": "web"}).Inc()
			publicServer.ServeHTTP(w, req)
		}
	})

	log.Println("listening on", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
