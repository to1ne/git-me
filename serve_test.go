package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIsPlainTextAgent(t *testing.T) {
	for _, tc := range []struct {
		desc     string
		agent    string
		expected bool
	}{
		{
			desc:     "Firefox",
			agent:    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:101.0) Gecko/20100101 Firefox/101.0",
			expected: false,
		},
		{
			desc:     "curl",
			agent:    "curl/7.83.1",
			expected: true,
		},
		{
			desc:     "git",
			agent:    "git/2.36.1",
			expected: false,
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			actual := isPlainTextAgent(tc.agent)
			require.Equalf(t, tc.expected, actual, "User agent mismatch")
		})
	}
}
