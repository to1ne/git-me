server := bin/serve
builder := bin/build

export CGO_ENABLED := 0

all: $(addprefix public/,css/git-me.css install.sh index.html toon.git plain/README plain/install.sh)

test: public/.dummy
	go test ./...

deploy:
	fly deploy

.INTERMEDIATE: public/.dummy
public/.dummy: public/
	touch $@

clean:
	git clean -Xfd

%/:
	mkdir -p $@

public/%: %
	mkdir -p ${@D}
	cp $< $@

public/plain/%: %
	mkdir -p ${@D}
	cp $< $@

CI_COMMIT_SHA ?= source

public/toon.git: toon.git
	rm -rf $@
	cp -r $< $@
	git fast-export source | git -C $@ fast-import
	git -C $@ update-server-info

${builder}: bin/ $(wildcard cmd/build/*.go)
	go build -o $@ ./cmd/build

${server}: bin/ $(wildcard *.go) $(wildcard public/*)
	go build -o $@ .

public/index.html: ${builder} public/
	$<

serve: ${server}
	$<

.PHONY: all test clean serve
