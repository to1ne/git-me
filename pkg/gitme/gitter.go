package gitme

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"time"
)

type Gitter struct {
	path        string
	authorName  string
	authorEmail string
	extraEnv    []string
}

// NewGitter creates a Gitter object operating at the given path.
// It initializes a new, bare, git repo at that path.
func NewGitter(repoPath string) Gitter {
	return Gitter{path: repoPath}
}

func (g *Gitter) Init() error {
	if err := os.RemoveAll(g.path); err != nil {
		return fmt.Errorf("NewGitter: %v", err)
	}

	cmd := exec.Command("git", "init",
		"--bare",
		"--object-format=sha256",
		g.path)

	return cmd.Run()
}

func (g *Gitter) SetAuthor(name, email string) {
	g.authorName = name
	g.authorEmail = email
}

// DefaultTree creates the default containing the README and
// returns the Oid of it.
func (g Gitter) DefaultTree() (Oid, error) {
	return g.runOid("mktree")
}

func (g Gitter) CreateCommit(date time.Time, msg string, tree Oid, parentOid ...Oid) (Oid, error) {
	args := []string{"-m", msg, tree.String()}
	for _, poid := range parentOid {
		args = append(args, "-p", poid.String())
	}
	timestamp := fmt.Sprintf("%d", date.Unix())

	extraEnv := []string{
		"GIT_COMMITTER_DATE=" + timestamp + " +0100",
		"GIT_AUTHOR_DATE=" + timestamp + " +0100",
	}
	commit, err := g.withEnv(extraEnv).runOid("commit-tree", args...)
	if err != nil {
		return NoOid, fmt.Errorf("CreateCommit: %v", err)
	}

	return commit, nil
}

func (g Gitter) CreateBranch(name string, oid Oid) error {
	_, err := g.run("update-ref", "refs/heads/"+name, oid.String())

	return err
}

func (g Gitter) SetDefaultBranch(name string) error {
	_, err := g.run("symbolic-ref", "HEAD", "refs/heads/"+name)

	return err
}

// withEnv will return a Gitter object that injects extra environment variables
// into the environment when running a command.
func (g Gitter) withEnv(env []string) Gitter {
	gitter := g

	gitter.extraEnv = env

	return gitter
}

func (g Gitter) cmd(cmd string, cmdArgs ...string) *exec.Cmd {
	args := []string{"--git-dir", g.path, cmd}
	args = append(args, cmdArgs...)

	c := exec.Command("git", args...)

	c.Env = append(os.Environ(),
		"GIT_AUTHOR_NAME="+g.authorName,
		"GIT_AUTHOR_EMAIL="+g.authorEmail,
		"GIT_COMMITTER_NAME="+g.authorName,
		"GIT_COMMITTER_EMAIL="+g.authorEmail)
	if len(g.extraEnv) > 0 {
		c.Env = append(c.Env, g.extraEnv...)
	}

	return c
}

func (g Gitter) run(cmd string, cmdArgs ...string) ([]byte, error) {
	c := g.cmd(cmd, cmdArgs...)

	b, err := c.Output()
	if err != nil {
		var exit *exec.ExitError
		if errors.As(err, &exit) {
			return b, fmt.Errorf("command [%s] failed: %v", c.String(), string(exit.Stderr))
		}
	}

	return b, err
}

func (g Gitter) runOid(cmd string, args ...string) (Oid, error) {
	b, err := g.run(cmd, args...)
	if err != nil {
		return NoOid, err
	}
	return OidFromBytes(b), nil
}
