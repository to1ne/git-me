package gitme

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"

	"git-me.tech/pkg/gitsvg"
	"git-me.tech/pkg/hyperdiesel"
	"git-me.tech/pkg/hyperdiesel/css"
)

// laneTaker has information about who is taking the lane.
type laneTaker struct {
	// color of the line.
	color string
	// prev holds reference to the previous commit (that has been drawn).
	prev Commit
	// head holds the topmost commit. This acts as the name for the branch.
	headOid Oid
	// nextOid holds oid name of next commit (to be drawn).
	nextOid Oid
	// active indicates if the lane is in drawing mode already
	active bool
}

func (lt laneTaker) cssClass() string {
	return "lane-" + lt.headOid.String()
}

func (lt laneTaker) String() string {
	return fmt.Sprintf("laneTaker{ head: %v, next: %v}", lt.headOid, lt.nextOid)
}

// htmlizer holds bookkeeping for htmlizing a commit graph.
type htmlizer struct {
	// lanes are the active lanes in use.
	lanes       []*laneTaker
	prevArticle *hyperdiesel.Element
	spanLanes   []string
	visLanes    []string
	stylesheet  *hyperdiesel.StyleSheet
	ancestorMap map[Oid][]Oid
	colorInd    int
}

func (h *htmlizer) laneCount() int {
	return len(h.lanes)
}

// lastLane returns the last known lane and true, or nil and false.
func (h *htmlizer) lastLane() (*laneTaker, bool) {
	if h.laneCount() == 0 {
		return nil, false
	}
	return h.lanes[h.laneCount()-1], true
}

func (h *htmlizer) formatCommit(commit Commit, el *hyperdiesel.Element) {
	details := el.Append("details").Attr("open", "open")
	h2 := details.Append("summary").Append("h2")

	h2.Append("abbr").Attr("title", commit.Oid.String()).
		Append("a").Attr("href", "#id-"+commit.Oid.String()).
		Write(commit.ShortOid())

	if len(commit.RefNames) > 0 {
		link := commit.RefNames[0]
		if strings.HasPrefix(link, "HEAD") {
			link = "index"
		}
		data := h2.Append("data").Attr("value", commit.RefNames[0])
		data.Append("a").Class("refs").
			Attr("href", link+".html").
			Write(commit.RefNames[0])
	}
	h2.Write(commit.Title)

	em := details.Append("em")
	em.Write("Date: ")
	em.Append("time").Attr("datetime", commit.Date).Write(commit.Date)

	h.htmlizeCommit(commit, details)
}

func readme(html *hyperdiesel.Element) error {
	f, err := os.Open("README")
	if err != nil {
		return fmt.Errorf("readme open: %w", err)
	}
	defer f.Close()

	c, err := io.ReadAll(f)
	if err != nil {
		return fmt.Errorf("readme read: %w", err)
	}

	r := strings.NewReplacer("<", "&lt;", ">", "&gt;")

	html.Append("aside").Append("pre").Adopt(hyperdiesel.Text(r.Replace(string(c))))

	return nil
}

func htmlFilename(name string) string {
	if name == "toon" {
		name = "index"
	}

	name = filepath.Join("public", name+".html")
	os.MkdirAll(path.Dir(name), os.ModePerm)

	return name
}

// Htmlize converts the git history at the given repoPath to HTML
func Htmlize(repoPath, branch string) error {
	htmlFile, err := os.Create(htmlFilename(branch))
	if err != nil {
		return fmt.Errorf("Htmlize: %w", err)
	}
	defer htmlFile.Close()

	rootPath := strings.Repeat("../", strings.Count(branch, "/"))

	doc, head := newDoc(branch, rootPath)
	body := doc.Html.Append("body")

	body.Append("code").Write("git me")
	// Don't put checkboxes inside another element, because otherwise the
	// CSS general sibling combinator (~) does not work.

	body.Append("input").Attr("type", "checkbox").Attr("id", "graph").Attr("checked", "checked")
	body.Append("label").Write("--graph").Attr("for", "graph")

	body.Append("input").Attr("type", "checkbox").Attr("id", "oneline")
	body.Append("label").Write("--oneline").Attr("for", "oneline")

	body.Append("input").Attr("type", "checkbox").Attr("id", "all")
	body.Append("label").Write("--all").Attr("for", "all")

	mainEl := hyperdiesel.New("main")

	h := &htmlizer{
		stylesheet:  hyperdiesel.NewStyleSheet(),
		ancestorMap: map[Oid][]Oid{},
	}

	logger, finish, err := NewLogger(repoPath, branch)
	if err != nil {
		return fmt.Errorf("Htmlize logger: %w", err)
	}
	defer finish()

	// Walk the commits reverse order
	for logger.Scan() {
		commit, err := logger.Commit()
		if err != nil {
			return fmt.Errorf("Htmlize scan commit: %w", err)
		}
		h.drawRow(commit, mainEl)

		// Build the ancestor map
		for _, parent := range commit.Parents {
			var ok bool
			var ancestors []Oid
			if ancestors, ok = h.ancestorMap[parent]; !ok {
				ancestors = []Oid{}
			}
			h.ancestorMap[parent] = append(ancestors, commit.Oid)
		}

		// Display the current commit if one of the parents is displayed
		dispVal := "none"
		visVal := "var(--vis)"
		for _, ancestor := range h.ancestorMap[commit.Oid] {
			dispVal = fmt.Sprintf("var(--disp-%v, %v)", ancestor.ShortOid(), dispVal)
			visVal = fmt.Sprintf("var(--vis-%v, %v)", ancestor.ShortOid(), visVal)
		}
		h.stylesheet.Rule("main").
			Declare(fmt.Sprintf("--disp-%v", commit.ShortOid()), dispVal).
			Declare(fmt.Sprintf("--vis-%v", commit.ShortOid()), visVal)

		h.stylesheet.Rule(".disp--"+commit.ShortOid()).
			Declare("display", fmt.Sprintf("var(--disp-%v,none)", commit.ShortOid()))

		// Add a checkbox for each ref
		for _, ref := range commit.RefNames {
			cssID := "ref-" + strings.ReplaceAll(ref, "/", "--")

			body.Append("input").Attr("type", "checkbox").Attr("id", cssID)
			body.Append("label").Write(ref).Attr("for", cssID)

			rule := h.stylesheet.Rule("#" + cssID + ":checked ~ main")
			rule.Declare("--disp-"+commit.ShortOid(), "var(--disp)")
			rule.Declare("--vis-"+commit.ShortOid(), "1")
		}
	}
	colStart := css.Sum(h.visLanes...)
	h.prevArticle.Style("grid-column-start", colStart)

	body.Append("hr")
	body.Adopt(mainEl)

	if branch != "toon" {
		body.Append("a").Attr("href", rootPath+"index.html").Write("↩ back")
	}

	if err := readme(body); err != nil {
		return fmt.Errorf("Htmlize: %w", err)
	}

	head.Append("style").Write(h.stylesheet.String())
	doc.PrettyPrint(htmlFile)

	if err = htmlFile.Sync(); err != nil {
		return fmt.Errorf("Htmlize: %w", err)
	}

	return nil
}

func newDoc(branch, rootPath string) (*hyperdiesel.Doc, *hyperdiesel.Element) {
	doc := hyperdiesel.Html5Doc()
	doc.Html.Attr("lang", "en")

	head := doc.Html.Append("head")
	head.Append("meta").Attr("charset", "utf-8").Void()
	head.Append("meta").Attr("name", "keywords").Attr("content", "Toon, Claes, Toon Claes, git, life history, resume").Void()
	head.Append("meta").Attr("name", "viewport").Attr("content", "width=device-width, initial-scale=1").Void()
	head.Append("meta").Attr("name", "description").Attr("content", "")

	head.Append("title").Write("git-me(1) %v -- Toon Claes", branch)
	head.Append("link").Attr("rel", "stylesheet").Attr("href", rootPath+"css/git-me.css").Void()
	head.Append("link").Attr("rel", "stylesheet").Attr("href", rootPath+"//cdn.jsdelivr.net/npm/hack-font@3.3.0/build/web/hack-subset.css").Void()
	head.Append("link").Attr("rel", "author").Attr("href", "humans.txt").Void()

	return doc, head
}

func (h *htmlizer) drawRow(commit Commit, mainEl *hyperdiesel.Element) {
	laneInd := -1
	visLanes := []string{"1"}

	dispClass := "disp--" + commit.ShortOid()

	// When event has multiple parents, lanes on the right need to
	// move over.
	for i, parent := range commit.Parents {
		if i == 0 {
			continue // Nothing special to do for first parent
		}
		if last, ok := h.lastLane(); !ok || last.nextOid == commit.Oid {
			break // No lane changes needed when in last lane
		}

		visLanes = []string{"1"} // Flush so we only keep results of last loop

		at := -1 // At which lane does the current commit live?
		for j, lane := range h.lanes {
			visLanes = append(visLanes, "var(--vis-"+lane.headOid.ShortOid()+")")
			div := mainEl.Div().Class(dispClass)

			if at < 0 {
				div.Adopt(gitsvg.Line().Class(lane.cssClass()))
			} else {
				div.Adopt(gitsvg.LaneChange().Class(lane.cssClass(), "lane-change"))
			}
			if lane.nextOid == commit.Oid {
				at = j
			}
		}

		// TODO filler class
		mainEl.Div().Class(dispClass)

		lane := h.NewLane(parent)
		visLanes = append(visLanes, "var(--vis-"+lane.headOid.ShortOid()+")")
		h.spanLanes = append(h.spanLanes, "var(--vis-"+lane.headOid.ShortOid()+")")

		if at < 0 {
			h.lanes = append(h.lanes, lane)
		} else {
			h.InsertLane(lane, at+1)
		}
	}

	if h.prevArticle != nil {
		rowSpan := css.Sum(h.spanLanes...)
		colStart := css.Max(
			css.Sum(h.visLanes...),
			css.Sum(visLanes...),
		)
		h.prevArticle.
			Style("grid-row", fmt.Sprintf("span %v", rowSpan)).
			Style("grid-column-start", colStart)
	}

	h.visLanes = []string{"1"}
	h.spanLanes = []string{"1"}

	// Draw the known lanes
	for i := 0; ; i++ {
		var top bool
		var lane *laneTaker

		// Add a lane if needed
		if laneInd < 0 && i == len(h.lanes) {
			lane = h.NewLane(commit.Oid)
			lane.active = true

			h.lanes = append(h.lanes, lane)

			top = true
		}
		// Exit the loop
		if len(h.lanes) <= i {
			break
		}

		lane = h.lanes[i]
		h.visLanes = append(h.visLanes, "var(--vis-"+lane.headOid.ShortOid()+")")

		// This lane is the lane for the current event
		if lane.nextOid == commit.Oid {
			var prevDiv *hyperdiesel.Element

			lane.active = true
			laneInd = i
			div := mainEl.Div().Class(dispClass)

			// Make sure every parent has a lane
			for j, p := range commit.Parents {
				if j == 0 {
					h.lanes[i].nextOid = p
					continue
				}

				var plane *laneTaker
				for _, l := range h.lanes {
					if p == l.nextOid {
						plane = l
						break
					}
				}

				if plane == nil {
					plane = h.NewLane(p)
					h.InsertLane(plane, laneInd+j)
				}
				plane.active = true

				h.visLanes = append(h.visLanes, "var(--vis-"+plane.headOid.ShortOid()+")")

				if prevDiv != nil {
					prevDiv.Predopt(gitsvg.HorLine().Class(plane.cssClass()))
				}

				div.Predopt(gitsvg.MergeIn().Class(plane.cssClass()))
				prevDiv = mainEl.Div().Class(dispClass).
					Adopt(gitsvg.MergeOut().Class(plane.cssClass()))

				// Skip over this child
				i += 1
			}

			// Draw the current
			if top {
				div.Adopt(gitsvg.Top().Class(lane.cssClass()))
			} else if 0 < len(commit.Parents) {
				div.Adopt(gitsvg.Between().Class(lane.cssClass()))
			} else {
				div.Adopt(gitsvg.Bottom().Class(lane.cssClass()))
			}
		} else if lane.active {
			mainEl.Div().Class(dispClass).
				Adopt(gitsvg.Line().Class(lane.cssClass()))
		}
	}

	h.prevArticle = mainEl.Article().
		Class(dispClass).
		Attr("id", "id-"+commit.Oid.String()) //.
	h.formatCommit(commit, h.prevArticle)

	// When event has no more parents, we can remove it's lane and the lanes
	// on the right need to move back in.
	if len(commit.Parents) == 0 {
		at := -1 // At which lane does the current commit live?

		for i, l := range h.lanes {
			if at < 0 {
				if l.nextOid == commit.Oid {
					at = i
					continue
				}

				mainEl.Div().Class(dispClass).
					Adopt(gitsvg.Line().Class(l.cssClass()))
			} else {
				mainEl.Div().Class(dispClass).
					Adopt(gitsvg.LaneChangeRev().Class(l.cssClass(), "lane-change"))
			}
		}
		// TODO filler
		mainEl.Div().Class(dispClass)

		h.spanLanes = append(h.spanLanes, "var(--vis-"+h.lanes[at].headOid.ShortOid()+")")

		h.lanes = append(h.lanes[:laneInd], h.lanes[laneInd+1:]...)
	}
}

// NewLane creates a new lane.
func (h *htmlizer) NewLane(oid Oid) *laneTaker {
	lane := &laneTaker{
		headOid: oid,
		nextOid: oid,
	}
	rule := h.stylesheet.Rule("svg."+lane.cssClass()+" circle", "svg."+lane.cssClass()+" path")
	rule.Declare("stroke", h.nextColor())
	//h.stylesheet.Rule("main").Declare("--vis-"+lane.headOid.ShortOid(), "1")

	return lane
}

// InsertLane at the given index.
func (h *htmlizer) InsertLane(lane *laneTaker, at int) {
	if at < 0 || len(h.lanes) == at {
		h.lanes = append(h.lanes, lane)
	} else {
		h.lanes = append(h.lanes[:at+1], h.lanes[at:]...)
		h.lanes[at] = lane
	}
}

// nextColor gives a the next available color.
func (h *htmlizer) nextColor() string {
	h.colorInd += 1

	// Only the main branch can be black
	if h.colorInd == 1 {
		return "black"
	}

	colors := []string{"maroon", "forestgreen", "gold", "dodgerblue",
		"mediumorchid", "darkturquoise", "crimson", "chartreuse",
		"khaki", "deepskyblue", "fuchsia", "paleturquoise",
	}

	return colors[h.colorInd%len(colors)]
}

func (h *htmlizer) htmlizeCommit(commit Commit, el *hyperdiesel.Element) {
	raw := commit.Body
	p := el.Append("p")
	var footnotes *hyperdiesel.Element

	for i := 0; i < len(raw); {
		switch {
		// Footnotes
		case len(raw[i:]) >= 5 && raw[i] == '[' && raw[i+2] == ']' && raw[i+3] == ':':
			if footnotes == nil {
				footnotes = hyperdiesel.New("ol")
			}
			note, rest, found := strings.Cut(raw[i+5:], "\n")
			if !found {
				panic("footnote malformed")
			}

			ref := commit.Oid.String() + "-" + raw[i+1:i+2]

			li := footnotes.Append("li").Attr("id", "footnote-"+ref)

			if strings.HasPrefix(note, "https://") {
				_, a := hyperlink(note)
				li.Adopt(a)
			} else {
				li.Write(note)
			}

			h.stylesheet.Rule("main:has(ol li#footnote-"+ref+":hover) p sup#footref-"+ref).Declare("background", "var(--light-fg)")

			raw = rest
			i = 0
		// Footnote references
		case len(raw[i:]) >= 3 && raw[i] == '[' && raw[i+2] == ']':
			p.Write(raw[:i])

			ref := commit.Oid.String() + "-" + raw[i+1:i+2]
			p.Append("sup").Write(raw[i:i+3]).Attr("id", "footref-"+ref)
			h.stylesheet.Rule("main:has(p sup#footref-"+ref+":hover) ol li#footnote-"+ref).Declare("background", "var(--light-fg)")

			raw = raw[i+3:]
			i = 0
		// New paragraphs
		case strings.HasPrefix(raw[i:], "\n\n"):
			p.Write(raw[:i])
			p = el.Append("p")
			raw = raw[i+2:]
			i = 0
		// Hyperlinks
		case strings.HasPrefix(raw[i:], "https://"):
			p.Write(raw[:i])
			l, a := hyperlink(raw[i:])
			p.Adopt(a)
			raw = raw[i+l:]
			i = 0
		default:
			i++
		}
	}
	p.Write(raw)
	if footnotes != nil {
		el.Adopt(footnotes)
	}
}

func hyperlink(from string) (int, *hyperdiesel.Element) {
	end := strings.IndexAny(from, " \n")
	if end < 0 {
		end = len(from)
	}
	if from[end-1] == '.' {
		end--
	}
	return end, hyperdiesel.New("a").Attr("href", from[:end]).Write(from[8:end])
}
