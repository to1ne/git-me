package gitme

import (
	"strings"
)

type Oid string

type Commit struct {
	Oid
	Parents  []Oid
	Date     string
	RefNames []string
	Title    string
	Body     string
}

const NoOid = Oid("")

func OidFromBytes(b []byte) Oid {
	return Oid(strings.TrimSpace(string(b)))
}

func (o Oid) String() string {
	return string(o)
}

func (o Oid) ShortOid() string {
	return o.String()[:7]
}

func (commit Commit) ShortOid() string {
	return commit.Oid.ShortOid()
}
