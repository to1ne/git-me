package hyperdiesel

import (
	"strings"
)

// Attr sets the named attribute to value.
func (el *Element) Attr(name, value string) *Element {
	el.attr[name] = Text(value)

	return el
}

// Class adds a css class to the `class` attribute.
// If you want to overwrite the `class` attribute instead,
// use `Attr("style", ...)` instead.
func (el *Element) Class(name ...string) *Element {
	attr, ok := el.attr["class"]
	if ok {
		name = append([]string{attr.String()}, name...)
	}

	return el.Attr("class", strings.Join(name, " "))
}
