package hyperdiesel

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestWrite(t *testing.T) {
	doc := Html5Doc()
	doc.Html.Attr("lang", "en")

	body := doc.Html.Append("body")
	el := body.Append("h1")
	el.Write("Title")
	el = body.Append("p")
	el.Write("This is the body")

	expected := `<!doctype html><html lang="en"><body><h1>Title</h1><p>This is the body</p></body></html>`

	require.Equal(t, expected, doc.String())
}
