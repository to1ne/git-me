// Package hyperdiesel helps you build an HTML & CSS document with code.
//
// The name comes from "hypertext", because HTML is a hypertext markup language,
// and "diesel" comes from DSL because that's how @ThePrimeagen likes to
// pronounce that.
package hyperdiesel
