package hyperdiesel

// Article creates a new <article> element.
func Article() *Element {
	return New("article")
}

// Article opens a new child <article> element.
func (el *Element) Article() *Element {
	return el.Append("article")
}

// Div creates a new <div> element.
func Div() *Element {
	return New("div")
}

// Div opens a new child <div> element.
func (el *Element) Div() *Element {
	return el.Append("div")
}
