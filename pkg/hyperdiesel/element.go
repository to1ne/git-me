package hyperdiesel

import (
	"fmt"
	"strings"
)

// Element defines an HTML element.
type Element struct {
	tag      string
	attr     map[string]fmt.Stringer
	parent   *Element
	children []fmt.Stringer
	void     bool
}

// New creates a new Element.
func New(tag string) *Element {
	return &Element{
		tag:  tag,
		attr: make(map[string]fmt.Stringer),
	}
}

// Write content to current element.
func (el *Element) Write(format string, a ...any) *Element {
	formatted := fmt.Sprintf(format, a...)
	el.children = append(el.children, Text(formatted))

	return el
}

// String returns the accumulated string.
func (el *Element) String() string {
	var level int

	for p := el.parent; p != nil; p = p.parent {
		level += 2
	}
	builder := strings.Builder{}

	builder.WriteString(el.openTag())

	for _, c := range el.children {
		builder.WriteString(c.String())
	}

	builder.WriteString(el.closeTag())

	return builder.String()
}

// Append a new subelement with name tag.
func (el *Element) Append(tag string) *Element {
	child := New(tag)
	el.Adopt(child)

	return child
}

// Adopt adds an existing element to the element's children.
func (el *Element) Adopt(child fmt.Stringer) *Element {
	el.children = append(el.children, child)
	if chel, ok := child.(*Element); ok {
		chel.parent = el
	}

	return el
}

// Predopt adds an existing element first to the element's children.
func (el *Element) Predopt(child fmt.Stringer) *Element {
	el.children = append([]fmt.Stringer{child}, el.children...)
	if chel, ok := child.(*Element); ok {
		chel.parent = el
	}

	return el
}

// Void marks the element a void element without children and no close tag.
func (el *Element) Void() *Element {
	el.void = true

	return el
}

// Parent returns the parent element. Might be nil.
func (el *Element) Parent() *Element {
	return el.parent
}

func (el *Element) FirstChild() (*Element, bool) {
	if len(el.children) == 0 {
		return nil, false
	}

	child, ok := el.children[0].(*Element)

	return child, ok
}

// openTag formats the opening tag.
func (el *Element) openTag() string {
	var builder strings.Builder

	builder.WriteString("<")
	builder.WriteString(el.tag)

	for n, v := range el.attr {
		builder.WriteString(fmt.Sprintf(` %v="%v"`, n, v))
	}

	// Self-closing tags are required in void elements in SVG.
	// https://developer.mozilla.org/en-US/docs/Glossary/Void_element#self-closing_tags
	var selfClose bool
	for p := el.parent; p != nil; p = p.parent {
		if p.tag == "svg" {
			selfClose = true
			break
		}
	}

	if selfClose {
		builder.WriteString(" />")
	} else {
		builder.WriteString(">")
	}

	return builder.String()
}

// closeTag formats the closing tag. Might be an empty string.
func (el *Element) closeTag() string {
	if el.void {
		return ""
	}

	return fmt.Sprintf("</%v>", el.tag)
}

func (el *Element) inline() bool {
	return el.tag == "a" || el.tag == "p"
}
