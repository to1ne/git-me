// Package gitsvg has a set of svg paths that can be used to draw an git graph
// in svg.
package gitsvg

import "git-me.tech/pkg/hyperdiesel"

// Svg returns an empty <svg> element as a hyperdiesel.Element.
func Svg() *hyperdiesel.Element {
	return hyperdiesel.New("svg").Attr("xmlns", "http://www.w3.org/2000/svg").
		Attr("viewBox", "0 0 32 32").
		Attr("preserveAspectRatio", "xMinYMin")
}

// Knot returns the svg circle.
func Knot() *hyperdiesel.Element {
	return hyperdiesel.New("circle").
		Attr("cx", "16").
		Attr("cy", "16").
		Attr("r", "5").Void()
}

// Top returns an svg element for the topmost commit in a graph, a commit that
// has no parents.
func Top() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M 16 16 v 1000").Void()
	svg.Adopt(Knot())

	return svg
}

// Between returns an svg element for a commit in between two other commits.
func Between() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M 16 0 v 1000").Void()
	svg.Adopt(Knot())

	return svg
}

// Bottom returns an svg element for the bottom commit in a graph, a commit that has no
// children.
func Bottom() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M 16 0 v 16").Void()
	svg.Adopt(Knot())

	return svg
}

// Line returns an svg element connecting two commits.
func Line() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M 16 0 v 1000").Void()

	return svg
}

// HorLine returns an svg element connecting a third or higher parent
func HorLine() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M -16 26 h 48").Void()

	return svg
}

// MergeIn returns an svg element which connects a secondary parent to a commit.
func MergeIn() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M 18 18 c 0 4, 4 8, 8 8 s 8 0, 8 0").Void()

	return svg
}

// MergeOut returns an svg element which connects a secondary parent to it's
// branch.
func MergeOut() *hyperdiesel.Element {
	svg := Svg()

	svg.Append("path").Attr("d", "M 0 26 c 8 0, 8 0, 8 0 s 8 0, 8 8 s 0 1000, 0 1000").Void()

	return svg
}

func LaneChange() *hyperdiesel.Element {
	svg := Svg().Attr("viewBox", "0 0 64 32").
		Attr("preserveAspectRatio", "xMinYMax")

	svg.Append("path").Attr("d", "M 16 -1000 C 16 0, 16 0, 16 0 c 0 16, 32 16, 32 32").Void()

	return svg
}

func LaneChangeRev() *hyperdiesel.Element {
	svg := LaneChange()

	if path, ok := svg.FirstChild(); ok {
		path.Attr("transform", "scale(-1, 1) translate(-64, 0)")
		//path.Attr("transform", "scale(1, -1)")
	}

	return svg
}

const BendBottomLeft = ``

const BendBottomRight = ``
